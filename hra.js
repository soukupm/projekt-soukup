function jas(){
    document.getElementById("logo_nadpis").style.filter ="brightness(150%)"
}
function jas_konec(){
    document.getElementById("logo_nadpis").style.filter ="brightness(100%)"
}
//Tohle je funkce, která zprovozňuje interaktivní logo

let interval;
let konec=0;
let body=0;
function hra(){
    body=0;
    konec = 0;
    vyloucene_hodnoty.length = 0
    document.getElementById("čas").textContent = "čas:"
    clearInterval(interval)
    console.log("funkce hra")
    document.getElementById("tabulka").style.display = "block";
    document.getElementById("play").style.display = "none";
    document.getElementById("test").style.display = "block";
    document.getElementById("body").textContent = "body: "+body+"/10";
    interval = setInterval(cas,1000);
    kolo()

}
//Tohle je hlavní funkce, která se spustí při kliknutí na tlačítko

let cislo = 0;
let hodnoty= [1,2,3,4,5,6,7,8,9,10];
let vyloucene_hodnoty=[];
let správná_odpověď=0;
let odpověď=0;
let porovnani_interval;
const hitboxes = document.getElementsByClassName("hitbox");

function kolo(){
    
    for (const hitbox of hitboxes) {
        hitbox.style.backgroundColor = "red";
    }
    odpověď=0;
    console.log("funkce kolo")
    while(true){
        console.log("Vyloučené hodnoty jsou:",vyloucene_hodnoty,"a celkové hodnoty jsou:",hodnoty)
        if(vyloucene_hodnoty.length === hodnoty.length && vyloucene_hodnoty.every((value, index) => value === hodnoty[index])){ //Tahle funkce porovnává dvě pole tím, že napřed porovná délku, a následně každou hodnotu na konkrétním indexu.
            console.log("konec cyklu (konec hry, protože vyloučené hodnoty = hodnoty)")
            konec=1;
            break;
        }
        else{
            cislo=random_cislo(1,10);
            console.log("generování čísla")
            console.log("číslo je.",cislo)
            if (vyloucene_hodnoty.includes(cislo)==true){
                console.log("vyloučené hodnoty jsou:",vyloucene_hodnoty, "a číslo je:",cislo)
                console.log("znovu(cislo je ve vyloučených hodnotách)")
                continue}
            else{
                console.log("vygenerovalo se číslo, které není ve vyloučených hodnotách")
                console.log(cislo)
                break}  
        }
       }
       
       if(konec == 1){
        clearInterval(porovnani_interval);
        console.log("konec funkce kolo")
        document.getElementById("play").style.display = "block";
        document.getElementById("test").style.display = "none";
        document.getElementById("tabulka").style.display = "none";
        alert("konec")
        return;
       }
    
        switch(cislo){
            case 1:
                document.getElementById("vlajka").src = "Flag_of_the_Czech_Republic.svg.png";
                document.getElementById("nazev").textContent = "Česko";
                vyloucene_hodnoty.push(1);
                správná_odpověď=1;

                break;
            case 2:
                document.getElementById("vlajka").src = "Flag_of_germany_800_480.png";
                document.getElementById("nazev").textContent = "Německo";
                vyloucene_hodnoty.push(2);
                správná_odpověď=2;
                break;
                
            case 3:
                document.getElementById("vlajka").src = "flag-of-france-flag-of-italy-national-flag-france.jpg";
                document.getElementById("nazev").textContent = "Francie";
                vyloucene_hodnoty.push(3);
                správná_odpověď=3;
                break; 
            case 4:
                document.getElementById("vlajka").src = "belgie.png";
                document.getElementById("nazev").textContent = "Belgie";
                vyloucene_hodnoty.push(4);
                správná_odpověď=4;
                break;
            case 5:
                document.getElementById("vlajka").src = "slovensko.png";
                document.getElementById("nazev").textContent = "Slovensko";
                vyloucene_hodnoty.push(5);
                správná_odpověď=5;
                break;
            case 6:
                document.getElementById("vlajka").src = "polsko.png";
                document.getElementById("nazev").textContent = "Polsko";
                vyloucene_hodnoty.push(6);
                správná_odpověď=6;
                break;
            case 7:
                document.getElementById("vlajka").src = "Italy.png";
                document.getElementById("nazev").textContent = "Itálie";
                vyloucene_hodnoty.push(7);
                správná_odpověď=7;
                break;
            case 8:
                document.getElementById("vlajka").src = "Rusko.png";
                document.getElementById("nazev").textContent = "Rusko";
                vyloucene_hodnoty.push(8);
                správná_odpověď=8;
                break;
            case 9:
                document.getElementById("vlajka").src = "Ukraine.png";
                document.getElementById("nazev").textContent = "Urkajina";
                vyloucene_hodnoty.push(9);
                správná_odpověď=9;
                break;
            case 10:
                document.getElementById("vlajka").src = "Spain.png";
                document.getElementById("nazev").textContent = "Španělsko";
                vyloucene_hodnoty.push(10);
                správná_odpověď=10;
                break;
        }
        console.log("proběhl switch")
        console.log("vyloučetné hodnoty:",vyloucene_hodnoty)
        vyloucene_hodnoty.sort((a, b) => a - b); //Tady ta část uspořádává pole, podle hodnot čísel. (samotné .sort() pracuje s čísly jako se stringy, což bylo v pohodě
        // do té doby, než se nepřidá do pole číslo 10, které se považuje jako menší než 2, protože začíná na číslici "1")
        //Přidal jsem tam funkci (a, b) => a - b ,která vrátí buď kladnou/zápornou/nulovou hodnotu a podle toho se pokaždé ty 2 hodnoty (vedle sebe) buď seřadí, nebo se nestane nic (když budou obě hodnoty stejné a funkce vrátí nulu)
        //funkce pokračuje do té doby, než se neuspořádá pole
        console.log("seřadili se vyloučené hodnoty")
        console.log("vyloučené hodnoty:",vyloucene_hodnoty)
        cislo = 0;
        console.log("číslo se vynulovalo")
        console.log("")

        porovnani_interval=setInterval(porovnat,50);
        if(konec==1){
            clearInterval(porovnani_interval);
        }
       
}

//tahle funkce se stará o jednotlivá "kola" hry

function porovnat(){
    console.log("zjišťuje se, jestli jestli je odpověď větší než 0")
    console.log("odpověď je: ",odpověď)
    console.log("správná odpověď je: ",správná_odpověď)
if(odpověď>0){
    console.log("odpověď je větší než 0")
    porovnání()
    clearInterval(porovnani_interval);
}
else if(odpověď===0){
     console.log("čeká se na odpověď")
}
else{
    console.log("chyba")
}
}

function porovnání(){
    console.log("aktivovala se funkce porovnání")
    console.log("odpověď je: ",odpověď)
    console.log("správná odpověď je: ",správná_odpověď)
    if(odpověď===správná_odpověď){
        body++
        console.log("přidali se body")
        console.log("")
        console.log("")
        console.log("")
    }
    else{
        switch(správná_odpověď){
            case 1:
                document.getElementById("Česko").style.backgroundColor= "green";
                break;
            case 2:
                document.getElementById("Německo").style.backgroundColor= "green";
                break;
            case 3:
                document.getElementById("Francie").style.backgroundColor= "green";
                break;
            case 4:
                document.getElementById("Belgie").style.backgroundColor= "green";
                break;
            case 5:
                document.getElementById("Slovensko").style.backgroundColor= "green";
                break;
            case 6:
                document.getElementById("Polsko").style.backgroundColor= "green";
                break;
            case 7:
                document.getElementById("Itálie").style.backgroundColor= "green";
                break;
            case 8:
                document.getElementById("Rusko").style.backgroundColor= "green";
                break;
            case 9:
                document.getElementById("Ukrajina").style.backgroundColor= "green";
                break;
            case 10:
                document.getElementById("Španělsko").style.backgroundColor= "green";
                break;
        }
    }
     document.getElementById("body").textContent = "body: "+body+"/10"
}
//Tahle funkce porovnává odpověď uživatele se správnou odpovědí a přidává body, nebo ukazuje, kde je správná odpověď.

function Česko(){
    odpověď=1;
}
function Německo(){
    odpověď=2;
}
function Francie(){
    odpověď=3;
}
function Belgie(){
    odpověď=4;
}
function Slovensko(){
    odpověď=5;
}
function Polsko(){
    odpověď=6;
}
function Itálie(){
    odpověď=7;
}
function Rusko(){
    odpověď=8;
}
function Ukrajina(){
    odpověď=9;
}
function Španělsko(){
    odpověď=10;
}

function random_cislo(minimum, maximum) {
    return Math.floor(Math.random() * (maximum - minimum + 1)) + minimum;
  }
//Tahle funkce vrací náhodné číslo od minima do maxima (můžeme zadat růuná čísla)

let sekundy = 0;
let minuty = 0;
function cas(){
    sekundy++;
    if(sekundy==60){
        sekundy = 0;
        minuty = 1;
    }
    document.getElementById("čas").textContent = "čas: "+minuty+"m "+sekundy+"s";
    
}
//Tahle funkce vypysuje čas od spušťení hry